pipeline {

  agent any

  environment {
    DB_HOST_TEST = 'mongo-server'
    DB_PUERTO_TEST = '27017'
    GCP_PROJECT_ID = 'nwo-jbdevops-2021'
    // GCP_ARTIFACT_REGISTRY_REGION = ''
    GCP_ARTIFACT_REGISTRY_REGION = 'us-west3-docker.pkg.dev'
    GCP_ARTIFACT_REGISTRY_REPOSITORY = "${env.BRANCH_NAME == 'develop' ? 'desarrollo' : 'testing'}"
    GCP_GCLOUD_PATH = "/opt/google-cloud-sdk/bin"
    GCP_SERVICE_JENKINS = credentials("service-jenkins")
    // us-west3-docker.pkg.dev/nwo-jbdevops-2021/desarrollo
  }

  tools {
    maven 'Maven 3.8.1'
    jdk 'jdk11'
  }
  

  stages {
    
    stage('compilar') {
      steps {
        echo 'Compilando...'
        sh "mvn clean compile"
      }
    }

    stage('Pruebas') {
      steps {
        echo 'Ejecutando pruebas unitarias e Integrales...'
        sh "mvn test -Dspring.profiles.active=test"
      }
      post {
        success {
          junit "target/surefire-reports/*.xml"
        }
      }
    }

    stage('Analisis de codigo estatico') {
      steps{
        echo 'Analizando codigo...'

        withSonarQubeEnv('sonarqube-server') {
          echo "${BUILD_NUMBER}"
          sh "mvn sonar:sonar -Dsonar.host.url=http://sonarqube:9000 -Dsonar.projectVersion=${BUILD_NUMBER} "
        }

        timeout(time: 6, unit:'MINUTES') {
          waitForQualityGate abortPipeline: true
        }

      }
    }

    stage('Build') {
            steps {
                echo 'Generando build...'
                sh "mvn package -DskipTests"
                // sh "docker build -t nwoswo/servicio-seguridad:$BUILD_NUMBER ."
                sh "docker build -t $GCP_ARTIFACT_REGISTRY_REGION/$GCP_PROJECT_ID/$GCP_ARTIFACT_REGISTRY_REPOSITORY/servicio-seguridad:$BUILD_NUMBER ."

            }
        }

    stage('Publicar Build'){
      steps {
        echo 'Publicando imagen en google artifact registry ...'
        sh "docker login -u _json_key --password-stdin https://$GCP_ARTIFACT_REGISTRY_REGION < $GCP_SERVICE_JENKINS"

        sh "docker push $GCP_ARTIFACT_REGISTRY_REGION/$GCP_PROJECT_ID/$GCP_ARTIFACT_REGISTRY_REPOSITORY/servicio-seguridad:$BUILD_NUMBER"
      }
      post{
          always{
              echo 'Eliminando imagenes locales'
              sh "docker rmi $GCP_ARTIFACT_REGISTRY_REGION/$GCP_PROJECT_ID/$GCP_ARTIFACT_REGISTRY_REPOSITORY/servicio-seguridad:$BUILD_NUMBER"
          }

      }
    }

    stage('Desplegar en Desarrollo'){
      when{
          branch 'develop'
      }
      steps{
          echo('Desplegando en ambiente desarrollo...')
          sh "$GCP_GCLOUD_PATH/gcloud auth activate-service-account service-jenkins@nwo-jbdevops-2021.iam.gserviceaccount.com --key-file=$GCP_SERVICE_JENKINS"
          sh "$GCP_GCLOUD_PATH/gcloud run deploy servicio-seguridad-dev --image=$GCP_ARTIFACT_REGISTRY_REGION/$GCP_PROJECT_ID/$GCP_ARTIFACT_REGISTRY_REPOSITORY/servicio-seguridad:$BUILD_NUMBER --region=us-west3 --port=8080  --min-instances=1 --allow-unauthenticated --set-env-vars='SPRING.PROFILES.ACTIVE=dev, DB_HOST=localhost, DB_PUERTO=27017' --project=$GCP_PROJECT_ID"
      }
    }

    stage('Desplegar en Testing') {
            when {
                branch 'testing'
            }
            steps {
                echo 'Desplegando en ambiente desarrollo...'
                sh "$GCP_GCLOUD_PATH/gcloud auth activate-service-account service-jenkins@nwo-jbdevops-2021.iam.gserviceaccount.com --key-file=$GCP_SERVICE_JENKINS"
                sh "$GCP_GCLOUD_PATH/gcloud run deploy servicio-seguridad-test --image=$GCP_ARTIFACT_REGISTRY_REGION/$GCP_PROJECT_ID/$GCP_ARTIFACT_REGISTRY_REPOSITORY/servicio-seguridad:$BUILD_NUMBER --region=us-west3 --port=8080  --min-instances=1 --allow-unauthenticated --set-env-vars='SPRING.PROFILES.ACTIVE=test, DB_HOST=localhost, DB_PUERTO=27017' --project=$GCP_PROJECT_ID"
            }
        }
        


// CONFIGURACION CON JENKINS LOCAL 
        // stage('Publicar Build') {
        //     steps {
        //         echo 'Publicando imagen en dockerhub'
        //         script {
        //             docker.withRegistry('', 'cnxDockerHub') {
        //                 docker.image("nwoswo/servicio-seguridad:$BUILD_NUMBER").push("$BUILD_NUMBER")
        //                 docker.image("nwoswo/servicio-seguridad:$BUILD_NUMBER").push("latest")
        //             }
        //         }
        //     }
        //     post {
        //         always {
        //             echo 'Eliminar imagenes locales'
        //             sh "docker rmi nwoswo/servicio-seguridad:$BUILD_NUMBER"
        //             sh "docker rmi nwoswo/servicio-seguridad:latest"
        //         }
        //     }
        // }

    
    

  }
}